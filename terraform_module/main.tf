resource "helm_release" "crd" {
  name            = var.crd_chart_name
  chart           = "https://github.com/arangodb/kube-arangodb/releases/download/${var.arangodb-operator-version}/kube-arangodb-crd-${var.arangodb-operator-version}.tgz"
  namespace       = var.namespace
  force_update    = var.crd_helm_force_update
  recreate_pods   = var.crd_helm_recreate_pods
  cleanup_on_fail = var.crd_helm_cleanup_on_fail
  max_history     = var.crd_helm_max_history

  values = var.crd_values
}

resource "helm_release" "operator" {
  name            = var.chart_name
  chart           = "https://github.com/arangodb/kube-arangodb/releases/download/${var.arangodb-operator-version}/kube-arangodb-${var.arangodb-operator-version}.tgz"
  namespace       = var.namespace
  depends_on      = [helm_release.crd]
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values
}
