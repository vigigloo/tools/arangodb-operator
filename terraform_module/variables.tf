variable "namespace" {
  type = string
}

variable "arangodb-operator-version" {
  type    = string
  default = "1.2.12"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "chart_name" {
  type    = string
  default = "arangodb-operator"

}
variable "crd_chart_name" {
  type    = string
  default = "arangodb-crd"
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "crd_values" {
  type    = list(string)
  default = []
}

variable "crd_helm_force_update" {
  type    = bool
  default = false
}

variable "crd_helm_recreate_pods" {
  type    = bool
  default = false
}

variable "crd_helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "crd_helm_max_history" {
  type    = number
  default = 0
}

